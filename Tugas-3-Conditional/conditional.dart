import 'dart:io';

// // SOAL NOMOR 1 (TERNARY OPERATOR)//
// void main(List<String> args) {
//   var check = false;
//   stdout.write("Apakah anda ingin menginstall aplikasi? (y/t): ");
//   var input = stdin.readLineSync();
//   (input == "y" || input == "Y") ? check = true : check = false;
//   check ? print("anda akan menginstall aplikasi dart") : print("aborted");
// }

// SOAL NOMOR 2 (IF-ELSE)//
// void main(List<String> args) {
//   var check = false;
//   stdout.write("Masukkan nama: ");
//   String? name = stdin.readLineSync();
//   if (name?.isEmpty == true) {
//     print("Nama tidak boleh kosong!");
//     main(args);
//   } else {
//     print(name);
//     check = true;
//   }

//   if (check) {
//     print("Halo $name, Pilih Peran mu!");
//     print("1. Penyihir");
//     print("2. Guard");
//     print("3. Warewolf");
//     print("Masukkan Pilihan mu (angka): ");
//     String? input = stdin.readLineSync();
//     int choose = int.parse(input!);
//     if (choose == 1) {
//       print("Nama: $name");
//       print("Peran: Penyihir");
//       print("Selamat Datang di dunia warewolf, $name");
//       print(
//           "Halo Penyihir $name, kamu dapat melihat siapa yang menjadi warewolf!");
//     } else if (choose == 2) {
//       print("Nama: $name");
//       print("Peran: Guard");
//       print("Selamat Datang di dunia warewolf, $name");
//       print(
//           "Halo Guard $name, kamu akan membantu melindungan temanmu dari serangan warewolf!");
//     } else if (choose == 3) {
//       print("Nama: $name");
//       print("Peran: Warewolf");
//       print("Selamat Datang di dunia warewolf, $name");
//       print("Halo Warewolf $name, kamu akan memakan mangsa setiap malam!");
//     } else {
//       print("Pilihan mu tidak ada, Memulai ulang Game!!");
//       main(args);
//     }
//   }
// }

// SOAL NOMER 3 (SWITCH CASE) //
// void main(List<String> args) {
//   print("1. Senin");
//   print("2. Selasa");
//   print("3. Rabu");
//   print("4. Kamis");
//   print("5. Jumat");
//   print("6. Sabut");
//   print("7. Minggu");
//   stdout.write("Masukkan hari (angka): ");
//   String? input = stdin.readLineSync();
//   int day = int.parse(input!);
//   switch (day) {
//     case 1:
//       print(
//           "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
//       break;
//     case 2:
//       print(
//           "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
//       break;
//     case 3:
//       print(
//           "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
//       break;
//     case 4:
//       print(
//           "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
//       break;
//     case 5:
//       print("Hidup tak selamanya tentang pacar.");
//       break;
//     case 6:
//       print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
//       break;
//     case 7:
//       print(
//           "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
//       break;

//     default:
//       main(args);
//   }
// }

// // SOAL NOMOR 4 (SWITCH CASE)//
// void main(List<String> args) {
//   var day = 1;
//   var month = 5;
//   var year = 1945;

//   switch (month) {
//     case 1:
//       print("$day Januari $year");
//       break;
//     case 2:
//       print("$day Februari $year");
//       break;
//     case 3:
//       print("$day Maret $year");
//       break;
//     case 4:
//       print("$day April $year");
//       break;
//     case 5:
//       print("$day Mei $year");
//       break;
//     case 6:
//       print("$day Juni $year");
//       break;
//     case 7:
//       print("$day Juli $year");
//       break;
//     case 8:
//       print("$day Agustus $year");
//       break;
//     case 9:
//       print("$day September $year");
//       break;
//     case 10:
//       print("$day Oktober $year");
//       break;
//     case 11:
//       print("$day November $year");
//       break;
//     case 12:
//       print("$day Desember $year");
//       break;

//     default:
//       print("ERROR: Bulan hanya berjumlah 12 bulan!!");
//       break;
//   }
// }
