import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 42),
          child: ListView(
            children: [
              SizedBox(
                height: 68,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Icon(
                      Icons.notifications,
                    ),
                  ),
                  SizedBox(
                    width: 25,
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Icon(Icons.add_shopping_cart),
                  ),
                ],
              ),
              SizedBox(
                height: 14,
              ),
              Text(
                "Welcome,",
                style: TextStyle(
                  color: Colors.lightBlue,
                  fontFamily: "Poppins",
                  fontSize: 40,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                "Hilmy",
                style: TextStyle(
                    color: Colors.blue[900],
                    fontFamily: "Poppins",
                    fontWeight: FontWeight.w400,
                    fontSize: 38),
              ),
              SizedBox(
                height: 68,
              ),
              Container(
                width: 278,
                height: 45,
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 72,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Recomended Place",
                    style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 120,
                    height: 70,
                    child: Image(
                      image: AssetImage("assets/img/Monas.png"),
                    ),
                  ),
                  Container(
                    width: 120,
                    height: 70,
                    child: Image(
                      image: AssetImage("assets/img/Roma.png"),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 14,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 120,
                    height: 70,
                    child: Image(
                      image: AssetImage("assets/img/Berlin.png"),
                    ),
                  ),
                  Container(
                    width: 120,
                    height: 70,
                    child: Image(
                      image: AssetImage("assets/img/Tokyo.png"),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
