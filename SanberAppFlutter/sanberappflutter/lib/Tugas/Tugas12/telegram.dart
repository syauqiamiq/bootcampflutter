import 'package:flutter/material.dart';
import 'package:sanberappflutter/Models/Chart_model.dart';
import 'package:sanberappflutter/Tugas/Tugas12/drawer.dart';

class Telegram extends StatefulWidget {
  const Telegram({Key? key}) : super(key: key);

  @override
  _TelegramState createState() => _TelegramState();
}

class _TelegramState extends State<Telegram> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Telegram"),
        actions: [
          Padding(
            padding: EdgeInsets.all(10),
            child: Icon(Icons.search),
          ),
        ],
      ),
      drawer: DrawerScreen(),
      body: SafeArea(
        child: ListView.separated(
          itemBuilder: (context, i) {
            return ListTile(
              leading: CircleAvatar(
                radius: 28,
                backgroundImage: NetworkImage(items[i].profileUrl),
              ),
              title: Text(
                items[i].name,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(items[i].message),
              trailing: Text(items[i].time),
            );
          },
          separatorBuilder: (context, i) {
            return Divider();
          },
          itemCount: items.length,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(
          Icons.create,
          color: Colors.white,
        ),
        backgroundColor: Color(0xFF65a9e0),
      ),
    );
  }
}
