import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sanberappflutter/Tugas/Tugas14/get_data.dart';

import 'Models/user_model.dart';

class PostDataApi extends StatefulWidget {
  const PostDataApi({Key? key}) : super(key: key);

  @override
  _PostDataApiState createState() => _PostDataApiState();
}

Future<UserModel?> createUser(
  String name,
  String email,
  String address,
) async {
  var apiUrl = Uri.parse("https://achmadhilmy-sanbercode.my.id/api/v1/profile");
  var response = await http.post(apiUrl, body: {
    "name": name,
    "email": email,
    "address": address,
  });
  if (response.statusCode == 201) {
    final String responseString = response.body;
    return userModelFromJson(responseString);
  } else {
    return null;
  }
}

class _PostDataApiState extends State<PostDataApi> {
  late UserModel _user;
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("POST DATA"),
      ),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: [
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: "Nama Lengkap",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                labelText: "Email",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              controller: addressController,
              decoration: InputDecoration(
                labelText: "Alamat",
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final String name = nameController.text;
          final String email = emailController.text;
          final String address = addressController.text;
          final UserModel? user = await createUser(name, email, address);
          setState(() {
            _user = user!;
          });
          Navigator.pop(context);
        },
        child: Icon(Icons.send),
      ),
    );
  }
}
