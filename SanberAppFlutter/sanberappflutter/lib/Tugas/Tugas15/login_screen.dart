import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas13/home_screen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/main_screen.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            SizedBox(
              height: 49,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Sanber Flutter",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w600,
                    color: Colors.lightBlue,
                    fontFamily: 'Poppins',
                  ),
                ),
                SizedBox(
                  height: 13,
                ),
                Container(
                  child: Image(image: AssetImage('assets/img/flutter.png')),
                  width: 93.8,
                  height: 100,
                ),
                SizedBox(
                  height: 29,
                ),
                Container(
                  width: 278,
                  height: 45,
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "Username",
                      hintStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(width: 1, color: Colors.red),
                          borderRadius: BorderRadius.circular(8)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 18.5,
                ),
                Container(
                  width: 278,
                  height: 45,
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      hintText: "Password",
                      hintStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(width: 0.5),
                          borderRadius: BorderRadius.circular(8)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 18.5,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Text(
                    "Forgot Password",
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        color: Colors.lightBlue,
                        fontSize: 14,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                  width: 278,
                  height: 48,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MainScreen()));
                    },
                    child: Text(
                      "Login",
                      style: TextStyle(
                          fontFamily: "Poppins",
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
                SizedBox(
                  height: 14,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Does not have account?",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: "Poppins",
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 23,
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Sign in",
                        style: TextStyle(
                          color: Colors.lightBlue,
                          fontFamily: "Poppins",
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 66,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 120,
                      height: 70,
                      child: Image(
                        image: AssetImage("assets/img/Berlin.png"),
                      ),
                    ),
                    SizedBox(
                      width: 25,
                    ),
                    Container(
                      width: 120,
                      height: 70,
                      child: Image(
                        image: AssetImage("assets/img/Tokyo.png"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
