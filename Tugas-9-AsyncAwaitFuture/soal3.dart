void main() async {
  print("Ready. Sing");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async {
  String lyric = "pernahkan kau merasa";
  final duration = Duration(seconds: 5);
  return Future.delayed(duration, () => lyric);
}

Future<String> line2() async {
  String lyric = "pernahkan kau merasa.....";
  final duration = Duration(seconds: 3);
  return Future.delayed(duration, () => lyric);
}

Future<String> line3() async {
  String lyric = "pernahkan kau merasa";
  final duration = Duration(seconds: 2);
  return Future.delayed(duration, () => lyric);
}

Future<String> line4() async {
  String lyric =
      "hatimu hampa pernahkah kau merasa hati mu kosong ............";
  final duration = Duration(seconds: 1);
  return Future.delayed(duration, () => lyric);
}
