import 'dart:async';

void main() {
  Human h = new Human();
  print("Luffy");
  print("Zoro");
  print("Killer");
  h.getData();
  print(h.name);
}

class Human {
  String name = "nama character one piece";
  Future<void> getData() async {
    final duration = Duration(seconds: 3);
    await Future.delayed(duration);
    name = "Hilmy";
    print("get name [done]");
    print(name);
  }
}
