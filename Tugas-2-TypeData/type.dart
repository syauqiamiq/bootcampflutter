import 'dart:io';

// ========= SOAL NOMOR 1 ========= //
// void main(List<String> args) {
//   var first = 'Dart';
//   var second = 'is';
//   var third = 'awesome';
//   var fourth = 'and';
//   var fifth = 'I';
//   var sixth = 'love';
//   var seventh = 'it!';
//   print("$first $second $third $fourth $fifth $sixth $seventh");
// }

// // ========= SOAL NOMOR 2 ========= //
// void main(List<String> args) {
//   var sentence = "I am going to be Flutter Developer";
//   var exampleFirstWord = sentence[0];
//   var exampleSecondWord = sentence[2] + sentence[3];
//   var thirdWord = sentence.substring(4, 10);
//   var fourthWord = sentence.substring(11, 13);
//   var fifthWord = sentence.substring(14, 16);
//   var sixthWord = sentence.substring(17, 24);
//   var seventhWord = sentence.substring(25, 34);

//   print('First Word: ' + exampleFirstWord);
//   print('Second Word: ' + exampleSecondWord);
//   print('Third Word: ' + thirdWord);
//   print('Fourth Word: ' + fourthWord);
//   print('Fifth Word: ' + fifthWord);
//   print('Sixth Word: ' + sixthWord);
//   print('Seventh Word: ' + seventhWord);
// }

// // // ========= SOAL NOMOR 3 ========= //
// void main(List<String> args) {
//   stdout.write("Masukkan nama depan: ");
//   var firstName = stdin.readLineSync();
//   stdout.write("Masukkan nama belakang: ");
//   var lastName = stdin.readLineSync();
//   print("Nama lengkap anda adalah: $firstName $lastName");
// }

// // // ========= SOAL NOMOR 4 ========= //
// void main(List<String> args) {
//   num a = 5;
//   num b = 10;
//   print('Perkalian : ${a * b}');
//   print('Pembagian : ${a / b}');
//   print('Penambahan : ${a + b}');
//   print('Pengurangan : ${a - b}');
// }
