import 'dart:io';
// SOAL NOMOR 1 //
// void main() {
//   print("LOOPING PERTAMA");
//   num count = 0;
//   while (count < 20) {
//     count += 2;
//     print("${count}. I Love Coding");
//   }
//   print("LOOPING KEDUA");
//   num loopSecond = 20;
//   while (loopSecond >= 2) {
//     print("${loopSecond}. I will become a mobile debeloper");
//     loopSecond -= 2;
//   }
// }

// SOAL NOMOR 2 //
// void main() {
//   num count = 20;
//   for (var i = 1; i <= count; i++) {
//     if (i % 2 == 0) {
//       print("$i. Berkualitas");
//     } else if (i % 3 == 0 && i % 2 != 0) {
//       print("$i. I Love Coding");
//     } else {
//       print("$i. Santai");
//     }
//   }
// }

// SOAL NOMOR 3 //
// void main() {
//   for (var i = 0; i < 4; i++) {
//     for (var j = 0; j < 8; j++) {
//       stdout.write("#");
//     }
//     stdout.writeln();
//   }
// }

// SOAL NOMOR 4 //
// void main() {
//   for (var i = 1; i <= 7; i++) {
//     for (var j = 0; j < i; j++) {
//       stdout.write("#");
//     }
//     stdout.writeln();
//   }
// }
