class Lingkaran {
  late double _diameter;
  set diameter(double value) {
    if (value < 0.0) {
      value *= -1;
    }
    _diameter = value;
  }

  double get luasLingkaran {
    var jari = _diameter / 2;
    var luas = jari * jari * 3.14;
    return luas;
  }
}
