import 'dart:io';

// SOAL NOMOR 1 //
// void main() {
//   print(range(1, 10));
// }

// range(startNum, endNum) {
//   if (startNum > endNum) {
//     List list = [];
//     for (var i = startNum; i >= endNum; i--) {
//       list.add(i);
//     }
//     return list;
//   } else if (startNum < endNum) {
//     List list = [];
//     for (var i = startNum; i <= endNum; i++) {
//       list.add(i);
//     }
//     return list;
//   }
// }

// SOAL NOMOR 2 //
// void main() {
//   print(rangeWithStep(5, 2, 1));
// }

// rangeWithStep(startNum, endNum, step) {
//   if (startNum > endNum) {
//     List list = [];
//     for (var i = startNum; i >= endNum; i -= step) {
//       list.add(i);
//     }
//     return list;
//   } else if (startNum < endNum) {
//     List list = [];
//     for (var i = startNum; i <= endNum; i += step) {
//       list.add(i);
//     }
//     return list;
//   }
// }

// SOAL NOMOR 3 //
// void main() {
//   var input = [
//     ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
//     ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
//     ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
//     ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
//   ];

//   dataHandling(input);
// }

// dataHandling(List n) {
//   for (var i = 0; i < n.length; i++) {
//     var id = n[i][0];
//     var name = n[i][1];
//     var address = n[i][2];
//     var dateOfBirth = n[i][3];
//     var hobby = n[i][4];
//     print('Nomor ID : $id');
//     print('Nama Lengkap : $name');
//     print('TTL : $address $dateOfBirth');
//     print('Hobby : $hobby');
//     print("");
//   }
// }

// SOAL NOMOR 4 //
// void main() {
//   print(balikKata("Kasur"));
//   print(balikKata("SanberCode"));
//   print(balikKata("Haji"));
//   print(balikKata("racecar"));
//   print(balikKata("Sanbers"));
// }

// balikKata(String input) {
//   var chars = input.split('');
//   return chars.reversed.join();
// }
