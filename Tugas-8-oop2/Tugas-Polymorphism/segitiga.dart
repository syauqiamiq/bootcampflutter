import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  late double _tinggi;
  late double _alas;
  Segitiga(double alas, double tinggi) {
    this._alas = alas;
    this._tinggi = tinggi;
  }

  @override
  double luas() {
    return (_alas * _tinggi) / 2;
  }

  @override
  double keliling() {
    print("DIASUMSIKAN SEGITIGA SAMA SISI");
    return (_alas * _alas * _alas);
  }
}
