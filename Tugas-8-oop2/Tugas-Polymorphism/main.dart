import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main() {
  BangunDatar bangunDatar = new BangunDatar();
  Segitiga segitiga = new Segitiga(5, 5);
  Lingkaran lingkaran = new Lingkaran(10);
  Persegi persegi = new Persegi(5);
  bangunDatar.luas();
  bangunDatar.keliling();
  print("");

  print("Luas Segitiga: ${segitiga.luas()}");
  print("");
  print("Keliling Segitiga: ${segitiga.keliling()}");
  print("");

  print("Luas Lingkaran: ${lingkaran.luas()}");
  print("Keliling Lingkaran: ${lingkaran.keliling()}");
  print("");

  print("Luas Persegi: ${persegi.luas()}");
  print("Keliling Persegi: ${persegi.keliling()}");
}
