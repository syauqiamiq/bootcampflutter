import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  late double _diameter;
  Lingkaran(double diameter) {
    this._diameter = diameter;
  }
  @override
  double luas() {
    var jari = _diameter / 2;
    return jari * jari * 3.14;
  }

  @override
  double keliling() {
    return _diameter * 3.14;
  }
}
