class Titan {
  late double _powerPoint;

  set powerPoint(double value) {
    if (value <= 5) {
      _powerPoint = 5;
    }
    _powerPoint = value;
  }

  double get powerPoint {
    return _powerPoint;
  }
}
