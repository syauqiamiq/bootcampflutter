import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';

void main() {
  AttackTitan attackTitan = new AttackTitan();
  ArmorTitan armorTitan = new ArmorTitan();
  BeastTitan beastTitan = new BeastTitan();

  attackTitan.powerPoint = 2;
  armorTitan.powerPoint = 10;
  beastTitan.powerPoint = 6;

  print("Power Point Attack Titan : ${attackTitan.powerPoint}");
  print("Power Point Armor Titan : ${armorTitan.powerPoint}");
  print("Power Point Beast Titan : ${beastTitan.powerPoint}");

  print("Skill Attack Titan: ${attackTitan.punch()}");
  print("Skill Armor Titan : ${armorTitan.terjang()}");
  print("Skill Beast Titan: ${beastTitan.lempar()}");
}
