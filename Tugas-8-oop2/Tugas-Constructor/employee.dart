class Employee {
  late int userId;
  late String name;
  late String department;

  Employee.userId(this.userId);
  Employee.name(this.name);
  Employee.department(this.department);
}
