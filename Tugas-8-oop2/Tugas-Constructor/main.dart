import 'employee.dart';

void main() {
  var userId = new Employee.userId(001);
  var name = new Employee.name("Oki");
  var department = new Employee.department("Informatika");

  print("ID: ${userId.userId}");
  print("Name: ${name.name}");
  print("Department: ${department.department}");
}
